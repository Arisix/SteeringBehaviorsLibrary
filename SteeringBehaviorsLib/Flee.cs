using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class Flee : MonoBehaviour {
    public float panicRadius = 3.5f;
    public bool decelerateOnStop = false;
    public float timeToTarget = 0.1f;

    float maxAcceleration { get; set; }

    private Rigidbody rb;
    private SteeringBasic sb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sb = GetComponent<SteeringBasic>();
        this.maxAcceleration = sb.maxAcceleration;
    }

    private Vector3 giveMaxAcceleration(Vector3 acceleration)
    {
    	acceleration.z = 0;
    	return acceleration.normalized * maxAcceleration;
    }

    public Vector3 getSteering(Vector3 targetPosition)
    {
    	Vector3 acceleration = rb.position - targetPosition;

    	if(acceleration.magnitude > panicRadius)
    	{
    		if(decelerateOnStop && rb.velocity.magnitude > 0.001f)
    		{
    			acceleration = -rb.velocity / timeToTarget;
                acceleration = (acceleration.magnitude > maxAcceleration) ? giveMaxAcceleration(acceleration) : acceleration;
                return acceleration;
    		}
    		else
    		{
    			return Vector3.zero;
    		}
    	}
    	
    	return this.giveMaxAcceleration(acceleration);
    }

    public Vector3 getSteering(Rigidbody target)
    {        
        return this.getSteering(target.position);
    }
}
