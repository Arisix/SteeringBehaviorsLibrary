using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class Arrival : MonoBehaviour {
    public float targetRadius = 1f;
    public float slowRadius = 1f;
    public float timeToDisired = 1f;

    float maxVolecity { get; set; }
    float maxAcceleration { get; set; }

    private SteeringBasic sb;
    private Rigidbody rb;

    void Start()
    {
        sb = GetComponent<SteeringBasic>();
        rb = GetComponent<Rigidbody>();
        this.maxVolecity = sb.maxVolecity;
        this.maxAcceleration = sb.maxAcceleration;
    }

    public Vector3 getSteering(Vector3 targetPosition)
    {
        Vector3 desiredVelosity = targetPosition - rb.position;
        desiredVelosity.z = 0;
        float distance = desiredVelosity.magnitude;

        if(distance < targetRadius) return Vector3.zero;

        float desiredSpeed;
        desiredSpeed = (distance > slowRadius) ? maxVolecity : maxVolecity * (distance / slowRadius);

        desiredVelosity = desiredVelosity.normalized * desiredSpeed;

        Vector3 acceleration = desiredVelosity;
        acceleration.z = 0;
        acceleration *= (1 / timeToDisired);
        acceleration = (acceleration.magnitude > maxAcceleration) ? acceleration.normalized * maxAcceleration : acceleration;

        return acceleration;
    }

    public Vector3 getSteering(Rigidbody target)
    {
        return this.getSteering(target.position);
    }

    public Vector3 Interpose(Rigidbody target1, Rigidbody target2)
    {
        Vector3 midPoint = (target1.position + target2.position) / 2;
        float timeToReachMidPoint = Vector3.Distance(transform.position, midPoint) / maxVolecity;
        midPoint = ((target1.position + target1.velocity) + (target2.position + target2.velocity) * timeToReachMidPoint) / 2;

        return this.getSteering(midPoint);
    }
}
