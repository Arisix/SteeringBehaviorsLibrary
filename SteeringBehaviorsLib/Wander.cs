using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Seek))]
public class Wander : MonoBehaviour {
    public float wanderOffset = 10f;
    public float wanderRadius = 10f;
    public float wanderRate = 0.5f;
    private float wnaderOrientation = 0;

    private Seek seek;

    void Start()
    {
        seek = GetComponent<Seek>();
    }

    public Vector3 getSteering()
    {
            float characterOrientation = transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
            wnaderOrientation += bonamialRandom() * wanderRate;

            float targetOrientation = wnaderOrientation + characterOrientation;
            Vector3 targetPosition = transform.position + (orientationToVector(characterOrientation)) * wanderOffset;

            targetPosition = targetPosition + (orientationToVector(targetOrientation) * wanderRadius);
            return seek.getSteering(targetPosition);
    }

    private float bonamialRandom()
    {
        return (Random.value - Random.value);
    }

    private Vector3 orientationToVector(float orientation)
    {
        return new Vector3(Mathf.Cos(orientation), Mathf.Sin(orientation), 0);
    }
}