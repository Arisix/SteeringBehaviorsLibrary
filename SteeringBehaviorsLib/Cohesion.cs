using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Arrival))]
public class Cohesion : MonoBehaviour {
    public float facingCosine = 120f;

    private float facingCosineValue;

    private SteeringBasic sb;
    private Arrival arrival;

    void Start()
    {
        sb = GetComponent<SteeringBasic>();
        arrival = GetComponent<Arrival>();
        facingCosineValue = Mathf.Cos(facingCosine * Mathf.Deg2Rad);
    }

    public Vector3 getSteering(ICollection<Rigidbody> targets)
    {
        Vector3 acceleration = Vector3.zero;
        int count = 0;

        foreach(Rigidbody target in targets)
        {
            if(sb.isFacing(target.position, facingCosineValue))
            {
                acceleration += target.position;
                count++;
            }
        }

        return (count == 0) ? Vector3.zero : arrival.getSteering(acceleration / count);
    }
}