using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Flee))]
public class Evade : MonoBehaviour {
    public float maxPredictRadius = 1f;
    public float reactSpeed = 0.9f;

    private Flee flee;

    void Start()
    {
        flee = GetComponent<Flee>();
    }

    public Vector3 getSteering(Rigidbody target)
    {
    	Vector3 displacement = target.position - transform.position;
    	float distance = displacement.magnitude;
    	float speed = target.velocity.magnitude;

    	float prediction = (speed <= distance / maxPredictRadius) ? maxPredictRadius : (distance / speed) * reactSpeed;

    	Vector3 explicitTarget = target.position + target.velocity * prediction;
    	return flee.getSteering(explicitTarget);
    }
}
