using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class UnalignedCollisionAvoidance : MonoBehaviour {
    public float facingCosine = 90f;
    public float timeToTarget = 0.1f;

    private float maxAcceleration;
    private float facingCosineValue;

    private SteeringBasic sb;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sb = GetComponent<SteeringBasic>();
        facingCosineValue = Mathf.Cos(facingCosine * Mathf.Deg2Rad);
        maxAcceleration = rb.maxAcceleration;
    }

    public Vector3 getSteering(ICollection<Rigidbody> targets)
    {
        Vector3 acceleration = Vector3.zero;
        int count = 0;

        foreach(Rigidbody target in targets)
        {
            if(sb.isFacing(target.position, facingCosineValue))
            {
                Vector3 matchAcceleration = (target.velocity - rb.velecity) / timeToTarget;

                acceleration += matchAcceleration;
                count++;
            }
        }

        acceleration = (count > 0) ? (acceleration / count) : acceleration;
        acceleration = (acceleration.magnitude > maxAcceleration) ? (acceleration.normalized * maxAcceleration) : acceleration;

        return acceleration;
    }
}