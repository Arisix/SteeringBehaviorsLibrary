using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Arrival))]
public class OffsetPursuit : MonoBehaviour {
    public float maxPredictRadius = 1f;

    private Arrival arrival;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        arrival = GetComponent<Arrival>();
    }

    public Vector3 getSteering(Rigidbody target, Vector3 offset, out Vector3 targetPos)
    {
        Vector3 offsetPosistion = target.position + target.transform.TransformDirection(offset);

        Vector3 displacement = offsetPosistion - transform.position;
        float distance = displacement.magnitude;

        float speed = rb.velocity.magnitude;
        float prediction;
        prediction = (speed <= distance / maxPredictRadius) ? maxPredictRadius : distance / speed;

        targetPos = offsetPosistion + target.velocity * prediction;
        return arrival.getSteering(targetPos);
    }

    public Vector3 getSteering(Rigidbody target, Vector3 offset)
    {
        Vector3 targetPos;
        return this.getSteering(target, offset, out targetPos);
    }
}
