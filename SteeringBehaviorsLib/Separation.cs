using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class Separation : MonoBehaviour {
    public float sepMaxAcceleration = 20f;
    public float sepMaxDistance = 10f;

    private float boundingRadius;

    private SteeringBasic sb;
    private Rigidbody rb;

    void Start()
    {
        sb = GetComponent<SteeringBasic>();
        rb = GetComponent<Rigidbody>();
        boundingRadius = sb.getBoundingRadius(this.transform);
    }

    public Vector3 getSteering(ICollection<Rigidbody> targets)
    {
        Vector3 acceleration = Vector3.zero;

        foreach(Rigidbody target in targets)
        {
            Vector3 direction = rb.position - target.position;
            float distance = direction.magnitude;

            if(distance < sepMaxDistance) 
            {
                float  targetRadius = sb.getBoundingRadius(target.transform);
                var strength = (sepMaxAcceleration * (sepMaxDistance - distance)) / (sepMaxDistance - (boundingRadius + targetRadius));
                acceleration = direction.normalized * strength;
            }
        }
        return acceleration;
    }
}