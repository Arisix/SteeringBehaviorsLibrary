using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Seek))]
public class Pursuit : MonoBehaviour {
    public float maxPredictRadius = 1f;

    private Seek seek;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        seek = GetComponent<Seek>();
    }

    public Vector3 getSteering(Rigidbody target)
    {
        Vector3 displacement = target.position - transform.position;
        float distance = displacement.magnitude;

        float speed = rb.velocity.magnitude;
        float predition = (speed <= distance / maxPredictRadius) ? maxPredictRadius : (distance / speed);

        Vector3 explicitTarget = target.position + target.velocity * predition;
        return seek.getSteering(explicitTarget);
    }
}
