using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class ObstacleAvoidance : MonoBehaviour {
    private float maxAcceleration;
    private float characterRadius;

    private SteeringBasic sb;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sb = GetComponent<SteeringBasic>();
        this.characterRadius = sb.getBoundingRadius(this.transform);
        this.maxAcceleration = sb.maxAcceleration;
    }

    public Vector3 getSteering(ICollection<Rigidbody> targets)
    {
        Vector3 acceleration = Vector3.zero;
        float shortestTime = float.PositiveInfinity;
        Rigidbody firstTarget = null;
        float firstMinSeparation = 0, firstDistance = 0, firstRadius = 0;
        Vector3 firstRelativePosition = Vector3.zero, firstRelativeVelocity = Vector3.zero;

        foreach(Rigidbody target in targets)
        {
            Debug.Log(target);
            Vector3 relativePosition = rb.position - target.position;
            Vector3 relativeVelocity = rb.velocity - target.velocity;
            float distance = relativePosition.magnitude;
            float relativeSpeed = relativeVelocity.magnitude;
            Debug.LogFormat("relativeSpeed : {0}", relativeSpeed);

            if(relativeSpeed == 0) continue;

            float timeToCollision = -1 * (Vector3.Dot(relativePosition, relativeVelocity) / (relativeSpeed * relativeSpeed));

            Vector3 separation = relativePosition + relativeVelocity * timeToCollision;
            float minSeparation = separation.magnitude;

            float targetRadius = sb.getBoundingRadius(target.transform);

            if(minSeparation > characterRadius + targetRadius) continue;

            if(timeToCollision > 0 && timeToCollision < shortestTime)
            {
                shortestTime = timeToCollision;
                firstTarget = target;
                firstMinSeparation = minSeparation;
                firstDistance = distance;
                firstRelativePosition = relativePosition;
                firstRelativeVelocity = relativeVelocity;
                firstRadius = targetRadius;
            }
        }
        if(firstTarget == null) return acceleration;

        Debug.Log(firstTarget);

        acceleration = (firstMinSeparation <= 0 || firstDistance < characterRadius + firstRadius) ? (transform.position - firstTarget.position) : (firstRelativePosition + firstRelativeVelocity * shortestTime);
        acceleration = acceleration.normalized * maxAcceleration;
        return acceleration;
    }
}
