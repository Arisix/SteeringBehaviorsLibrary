using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class SteeringBasic : MonoBehaviour {
    // Basic setting
    public float maxVolecity = 3.5f;
    public float maxAcceleration = 10f;

    // Rotation setting
    public float rotateSpeed = 10f;
    public bool smooth = true;
    public int numberOfSampleForSmooth = 5;
    private Queue<Vector2> velocitySamples = new Queue<Vector2>();

    // Near senser for collisionAoidance
    public HashSet<Rigidbody> targets = new HashSet<Rigidbody>();

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Steer(Vector3 acceleration)
    {
        rb.velocity = (acceleration.magnitude > maxAcceleration) ? acceleration.normalized * maxVolecity : acceleration;
    }

    public void Steer(Vector2 acceleration)
    {
        Steer(new Vector3(acceleration.x, acceleration.y, 0));   
    }

    public void lookAtDirection(float rotateAngle)
    {
        float rotation = Mathf.LerpAngle(transform.rotation.eulerAngles.z, rotateAngle, rotateSpeed * Time.fixedDeltaTime);
        transform.rotation = Quaternion.Euler(0, 0, rotation);
    }

    public void lookAtDirection(Vector2 direction)
    {
        direction.Normalize();
        if(direction.sqrMagnitude > 0.005f)
        {
            float rotateAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            lookAtDirection(rotateAngle);
        }
    }

    public void lookAtDirection(Quaternion rotateAngle)
    {
        lookAtDirection(rotateAngle.eulerAngles.z);
    }

    public void lookAtDirection()
    {
        Vector2 direction = rb.velocity;

        if(smooth)
        {
            if(velocitySamples.Count == numberOfSampleForSmooth) velocitySamples.Dequeue();
            velocitySamples.Enqueue(rb.velocity);
            direction = Vector2.zero;
            foreach(Vector2 v in velocitySamples) direction += v;
            direction /= velocitySamples.Count;
        }

        lookAtDirection(direction);
    }

    public bool isFacing(Vector3 target, float cosineValue)
    {
        Vector3 facing = transform.right.normalized;
        Vector3 directionToTarget = target - transform.position;

        return (Vector3.Dot(facing, directionToTarget) >= cosineValue);
    }

    public bool isInFront(Vector3 target)
    {
        return isFacing(target, 0);
    }

    public float getBoundingRadius(Transform target)
    {
        SphereCollider collider = target.GetComponent<SphereCollider>();
        return Mathf.Max(target.localScale.x, target.localScale.y, target.localScale.z) * collider.radius;
    }

    void OnTriggerEnter(Collider collider)
    {
        targets.Add(collider.GetComponent<Rigidbody>());
    }

    void OnTriggerExit(Collider collider)
    {
        targets.Remove(collider.GetComponent<Rigidbody>());
    }
}