using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SteeringBasic))]
public class Seek : MonoBehaviour {
    float maxVolecity { get; set; }
    float maxAcceleration { get; set; }

    private SteeringBasic sb;
    private Rigidbody rb;

    void Start()
    {
        sb = GetComponent<SteeringBasic>();
        rb = GetComponent<Rigidbody>();
        maxVolecity = sb.maxVolecity;
        maxAcceleration = sb.maxAcceleration;
    }

    public Vector3 getSteering(Vector3 targetPosition, float maxSeekAcceleration)
    {
        Vector3 acceleration = targetPosition - rb.position;
        acceleration.z = 0;
        acceleration = acceleration.normalized * maxSeekAcceleration;
        return acceleration;
    }

    public Vector3 getSteering(Vector3 targetPosition)
    {
        return this.getSteering(targetPosition, maxAcceleration);
    }

    public Vector3 getSteering(Rigidbody target)
    {
        return this.getSteering(target.position);
    }

    public Vector3 Interpose(Rigidbody target1, Rigidbody target2)
    {
        Vector3 midPoint = (target1.position + target2.position) / 2;
        float timeToReachMidPoint = Vector3.Distance(rb.position, midPoint) / maxVolecity;
        midPoint = ((target1.position + target1.velocity) + (target2.position + target2.velocity) * timeToReachMidPoint) / 2;

        return this.getSteering(midPoint);
    }
}
