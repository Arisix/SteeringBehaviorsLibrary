# Change Log

### 2018/08/06

> Class UnalighedCollisionAvoidance
+ Vector3 getSteering
	+ New class and function

> Class ObstacleAvoidance
+ Vector3 getSteering
	+ New class and function

---

### 2018/08/05
> Class SteeringBasic
+ HashSet<Rigidbody> targets = new HashSet<Rigidbody>();
	+ Add new attribute, nearsensor for detect other object
+ Vector3 Seek
	+ Remove Function, Replace by **Class Seek**
+ Vector3 Arrive
	+ Remove Function, Replace by **Class Arrival**
+ Vector3 Interpose
	+ Remove Function, Replace by **Vector3 Seek.Interpose** and **Vector3 Arrival.Interpose**
+ float getBoundingRadius
	+ New function

> Class Seek
+ Vector3 getSteering
	+ New class and function

> Class Arrival
+ Vector3 getSteering
	+ New class and function

> Class Seperation
+ Vector3 getSteering
	+ New class and function

> Class Cohesion
+ Vector3 getSteering
	+ New class and function

---

### 2018/08/04
> Class SteeringBasic
+ Vector3 Seek
	+ Add overload function **Seek(Rigidbody target)** 
+ Vector3 Arrive
	+ Fixed acceleration logical error
+ Vector3 Interpose
	+ Add **bool arriveOrSeek** parameter, default value is true
+ bool isFacing
	+ New function
+ bool isInFront
	+ New function

---

### 2018/08/03
> Class SteeringBasic
+ void Steer
+ Vector3 Seek
+ Vector3 Arrive
+ void lookAtDirection
+ Vector3 Interpose

> Class Flee
+ Vector3 getSteering

> Class Pursuit
+ Vector3 getSteering

> Class Evade
+ Vector3 getSteering

> Class Wander
+ Vector3 getSteering

---