﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeparationUnit : MonoBehaviour {
    SteeringBasic sb;
    Separation separation;
    GameObject red;
    // Use this for initialization
    void Start()
    {
        sb = GetComponent<SteeringBasic>();
        separation = GetComponent<Separation>();
        red = GameObject.Find("red");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 accelearation = separation.getSteering(sb.targets);

        sb.Steer(accelearation);
        sb.lookAtDirection();
    }
}
