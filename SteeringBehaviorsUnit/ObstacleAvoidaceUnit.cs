﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAvoidaceUnit : MonoBehaviour {
    SteeringBasic sb;
    ObstacleAvoidance obstacleavoidance;
    GameObject red;
    // Use this for initialization
    void Start () {
        sb = GetComponent<SteeringBasic>();
        obstacleavoidance = GetComponent<ObstacleAvoidance>();
        red = GameObject.Find("red");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 Accelearation = obstacleavoidance.getSteering(sb.targets);

        sb.Steer(Accelearation);
        sb.lookAtDirection();
	}
}
