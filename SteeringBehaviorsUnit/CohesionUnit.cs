﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CohesionUnit : MonoBehaviour {
    SteeringBasic sb;
    Cohesion cohesion;
    GameObject red;
    // Use this for initialization
    void Start () {
        sb = GetComponent<SteeringBasic>();
        cohesion = GetComponent<Cohesion>();
        red = GameObject.Find("red");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 accelearation = cohesion.getSteering(sb.targets);

        sb.Steer(accelearation);
        sb.lookAtDirection();
    }
}
