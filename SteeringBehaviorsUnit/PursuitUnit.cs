﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PursuitUnit : MonoBehaviour {
    Pursuit pursuit;
    SteeringBasic sb;
    GameObject red;

    // Use this for initialization
    void Start()
    {
        pursuit = GetComponent<Pursuit>();
        sb = GetComponent<SteeringBasic>();
        red = GameObject.Find("red");
    }

    // Update is called once per frame
    void Update () {
        Vector3 acceleration = pursuit.getSteering(red.GetComponent<Rigidbody>());

        sb.Steer(acceleration);
        sb.lookAtDirection();
	}
}
