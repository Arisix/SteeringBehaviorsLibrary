﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderUnit : MonoBehaviour {

    Wander wander;
    SteeringBasic sb;

	// Use this for initialization
	void Start () {
        wander = GetComponent<Wander>();
        sb = GetComponent<SteeringBasic>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration = wander.getSteering();

        sb.Steer(acceleration);
        sb.lookAtDirection();
    }
}
