﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeUnit : MonoBehaviour {
    SteeringBasic sb;
    Evade evade;
    GameObject red;

    // Use this for initialization
    void Start () {
        sb = GetComponent<SteeringBasic>();
        evade = GetComponent<Evade>();
        red = GameObject.Find("red");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration = evade.getSteering(red.GetComponent<Rigidbody>());

        sb.Steer(acceleration);
        sb.lookAtDirection();
	}
}
