﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivalUnit : MonoBehaviour {
    Arrival arrival;
    SteeringBasic sb;
    GameObject red;
    // Use this for initialization
    void Start()
    {
        arrival = GetComponent<Arrival>();
        sb = GetComponent<SteeringBasic>();
        red = GameObject.Find("red");
    }

    // Update is called once per frame
    void Update () {
        Vector3 acceleration = arrival.getSteering(red.transform.position);

        sb.Steer(acceleration);
        sb.lookAtDirection();
    }
}
