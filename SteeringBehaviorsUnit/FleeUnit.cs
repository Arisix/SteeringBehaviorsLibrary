﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeUnit : MonoBehaviour {
    Flee flee;
    SteeringBasic sb;
    private GameObject target;
    // Use this for initialization
    void Start () {
        flee = GetComponent<Flee>();
        sb = GetComponent<SteeringBasic>();
        target = GameObject.Find("red");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration = flee.getSteering(target.transform.position);

        sb.Steer(acceleration);
        sb.lookAtDirection();
    }
}
