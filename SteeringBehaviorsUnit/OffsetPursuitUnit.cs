﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetPursuitUnit : MonoBehaviour {
    SteeringBasic sb;
    OffsetPursuit offsetPursuit;
    GameObject red;

    Vector3 offset;


	// Use this for initialization
	void Start () {
        sb = GetComponent<SteeringBasic>();
        offsetPursuit = GetComponent<OffsetPursuit>();
        red = GameObject.Find("red");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration = offsetPursuit.getSteering(red.GetComponent<Rigidbody>(), offset);
        sb.Steer(acceleration);
        sb.lookAtDirection();
    }
}
