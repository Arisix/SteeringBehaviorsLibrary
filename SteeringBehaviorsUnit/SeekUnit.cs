﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekUnit : MonoBehaviour {
    SteeringBasic sb;
    Seek seek;
    GameObject red;
    // Use this for initialization
    void Start () {
        sb = GetComponent<SteeringBasic>();
        seek = GetComponent<Seek>();
        red = GameObject.Find("red");
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 acceleration = seek.getSteering(red.transform.position);
        sb.Steer(acceleration);
        sb.lookAtDirection();
    }
}
