# SteeringBehaviorsLibrary

steering behavior library for unity

## Reference

+ [**Steering Behaviors For Autonomous Characters**](https://www.red3d.com/cwr/steer/gdc99/) by Craig W. Reynolds
+ [**Understanding Steering Behaviors**](https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732) by Fernando Bevilacqua

## Complete list

+ [Seek](#seek)
+ [Flee](#flee)
+ [Arrival](#arrival)
+ [Pursuit](#pursuit)
+ [Evade](#evade)
+ [Wander](#wander)
+ [OffsetPursuit](#offsetpursuit)
+ [UnalighedCollisionAvoidance](#unalighedcollisionavoidance)
+ [ObstacleAvoidance](#obstacleavoidance)
+ [Cohesion](#cohesion)
+ [Separation](#separation)

## To do list

+ containment 
+ alignment
+ wall following
+ leader following
+ flow field following
+ path following

## Usage

### Seek

```c#
    SteeringBasic sb;
    Seek seek;
    Vector3 acceleration = seek.getSteering(target.transform.position);
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\SeekUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit/SeekUnit.cs)

---

### Flee

```c#
    SteeringBasic sb;
    Flee flee;
    Vector3 acceleration = flee.getSteering(target.transform.position);
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\Flee.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\Flee.cs)

---

### Arrival

```c#
    SteeringBasic sb;
    Arrival arrival;
    Vector3 acceleration = arrival.getSteering(target.transform.position);
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\ArrivalUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\ArrivalUnit.cs)

---

### Pursuit

```c#
    SteeringBasic sb;
    Pursuit pursuit;
    Vector3 acceleration = pursuit.getSteering(target.GetComponent<Rigidbody>());
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\PursuitUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\PursuitUnit.cs)

---

### Evade

```c#
    SteeringBasic sb;
    Evade evade;
    Vector3 acceleration = evade.getSteering(target.GetComponent<Rigidbody>());
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\EvadeUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\EvadeUnit.cs)

---

### Wander

```c#
    SteeringBasic sb;
    Wander wander;
    Vector3 acceleration = wander.getSteering();
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\WanderUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\WanderUnit.cs)

---

### OffsetPursuit

```c#
    SteeringBasic sb;
    OffsetPursuit offsetPursuit;
    Vector3 acceleration = offsetPursuit.getSteering();
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\OffsetPursuitUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\OffsetPursuitUnit.cs)

---

### ObstacleAvoidance

```c#
    SteeringBasic sb;
    ObstacleAvoidance obstacleavoidance;
    Vector3 acceleration = obstacleavoidance.getSteering(sb.targets);
    sb.Steer(acceleration);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\ObstacleAvoidaceUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\ObstacleAvoidaceUnit.cs)

---

### Separation

```c#
    SteeringBasic sb;
    Separation separation;
    Vector3 accelearation = separation.getSteering(sb.targets);
    sb.Steer(accelearation);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\ObstacleAvoidaceUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\SeparationUnit.cs)

---

### Cohesion

```c#
    SteeringBasic sb;
    Cohesion cohesion;
    Vector3 accelearation = cohesion.getSteering(sb.targets);
    sb.Steer(accelearation);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\ObstacleAvoidaceUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\CohesionUnit.cs)

---

### UnalighedCollisionAvoidance

```c#
    SteeringBasic sb;
    UnalighedCollisionAvoidance unalighedcollisionavoidance;
    Vector3 accelearation = unalighedcollisionavoidance.getSteering(sb.targets);
    sb.Steer(accelearation);
    sb.lookAtDirection();
```

Example : [SteeringBehaviorsUnit\UnalighedCollisionAvoidanceUnit.cs](https://gitlab.com/Irisix/SteeringBehaviorsLibrary/blob/master/SteeringBehaviorsUnit\UnalighedCollisionAvoidanceUnit.cs)

---
